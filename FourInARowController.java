import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class FourInARowController {
	//0 - red, 1 - blue
	private int playerTurn = 0;
	private final int rows = 5;
	private final int columns = 7;
	private int[][] gameMap = new int[rows][columns];
	private GridButton[] columnButtons = new GridButton[columns];
	
    @FXML
    private Button clearButton;

    @FXML
    private GridPane grid;
    
    @FXML
    private GridPane gridButtons;

    @FXML
    void clearGame(ActionEvent event) {
    	initialize();
    }
    
    private void cleanTable() {
    	playerTurn = 0;
    	grid.getChildren().clear();
    	gridButtons.getColumnConstraints().clear();
    	gridButtons.getChildren().clear();
    	gameMap = new int[rows][columns];
    	columnButtons = new GridButton[columns];
    }
    
    private void initGrid() {
    	for (int i = 0 ; i < rows ; i ++) {
    		for (int j =0 ; j < columns ; j++)
    		{
    			gameMap[i][j] = 0;
    		}
    	}    	
    }
    
    private void initGridButtons() {  	    	
    	// A quick fix for a weird appearance
        for (int i = 0; i < columns; i++) {
            ColumnConstraints colConstraints = new ColumnConstraints();
            colConstraints.setPercentWidth(100.0 / columns); // Divide the width equally among 7 columns
            gridButtons.getColumnConstraints().add(colConstraints);
        }
    	
		for (int j =0 ; j < columns ; j++)
		{
			columnButtons[j] = new GridButton(String.valueOf(j+1), 0, j);
			columnButtons[j].setMaxWidth(Double.MAX_VALUE);
			gridButtons.add(columnButtons[j], j, 0);
			columnButtons[j].setOnAction(new EventHandler<ActionEvent>() {
            	@Override
            	public void handle(ActionEvent event) {
            		handleClicked(event);
            	}
            });
		}
    }
    
    public void initialize() {
    	cleanTable();
    	initGrid();
    	initGridButtons();
    }
    
    private int drawCircle(int column) {
    	/*
    	 * returns the first (from the bottom) row number of the available cell
    	 * by choosing the cell the button draws a circle based on the turn
    	 */
    	for (int i = rows - 1 ; i >= 0 ; i--) {
    		if (gameMap[i][column] == 0) {
    			gameMap[i][column] = playerTurn + 1;
    			
                Circle circle = new Circle();
    			double cellHeight = grid.getHeight() / rows;
    			double cellWidth = grid.getWidth() / columns;
                double circleSize = Math.min(cellWidth, cellHeight) * 0.5;
                double circleCenterX = cellWidth / 2;
                double circleCenterY = cellHeight / 2;

                circle.setRadius(circleSize);
                circle.setCenterX(circleCenterX);
                circle.setCenterY(circleCenterY);
                
    			if (playerTurn + 1 == 1)
    				circle.setFill(Color.RED);
    			else
    				circle.setFill(Color.BLUE);
    			
    			grid.add(circle, column, i);
    			return i;
    		}
    	}
    	
    	return -1;
    }
    
    private String countDescendingDiagonalInARow(int selectedRow, int selectedColumn, int playerColor) {
    	// Diagonal like \
    	int count = 1;
    	Point currPoint = new Point(selectedRow, selectedColumn);
		// Ensure to keep the index in bounds of the buttons[][]
    	Point maximumRowAndMaximumColumnPoint = Utils.calcMaximumRowAndMaximumColumn(selectedRow, selectedColumn, rows, columns);
    	Point minimumRowAndMinimumColumnPoint = Utils.calcMinimumRowAndMinimumColumn(selectedRow, selectedColumn, rows, columns);
    	
    	for ( int index =  1; index < maximumRowAndMaximumColumnPoint.getY() - selectedColumn ; index++) {
			if (gameMap[currPoint.getX() + index][currPoint.getY() + index] != playerColor) {
				break;
			}
			count++;
    	}
    	
    	for ( int index =  1; index < selectedRow - minimumRowAndMinimumColumnPoint.getX() ; index++) {
			if (gameMap[currPoint.getX() - index][currPoint.getY() - index] != playerColor) {
				break;
			}
			count++;
    	}

    	return (count>=4) ? "Diagonal" : "";    		
    }
	
    private String countAscendingDiagonalInARow(int selectedRow, int selectedColumn, int playerColor) {
    	// Diagonal like /
    	int count = 1;
    	Point currPoint = new Point(selectedRow, selectedColumn);
		// Ensure to keep the index in bounds of the buttons[][]
    	Point maximumRowAndMinimumColumnPoint = Utils.calcMaximumRowAndMinimumColumn(selectedRow, selectedColumn, rows, columns);
    	Point minimumRowAndMaximumColumnPoint = Utils.calcMinimumRowAndMaximumColumn(selectedRow, selectedColumn, rows, columns);
    	    	
    	for ( int index =  1; index < maximumRowAndMinimumColumnPoint.getX() - selectedRow ; index++) {
			if (gameMap[currPoint.getX() + index][currPoint.getY() - index] != playerColor) {
				break;
			}
			count++;
    	}

    	for ( int index =  1; index < minimumRowAndMaximumColumnPoint.getY() - selectedColumn ; index++) {
			if (gameMap[currPoint.getX() - index][currPoint.getY() + index] != playerColor) {
				break;
			}
			count++;
    	}
    	
    	return (count>=4) ? "Diagonal" : "";    		
    }
   
    private String countVerticalInARow(int selectedRow, int selectedColumn, int playerColor) {
    	int index;
    	int count = 1;
    	
    	// for a 4 in a row in a vertical, the check starts for almost the top of the grid
    	if (selectedRow <= 1) {
    		for ( index = selectedRow + 1; index< selectedRow + 4 ; index++) {
    			if (gameMap[index][selectedColumn] != playerColor) {
    				return (count>=4) ? "Vertical" : "";
    			}
    			count++;
    		}
    	}
    	
    	return (count>=4) ? "Vertical" : "";
    }
    
    private String countHorizontalInARow(int selectedRow, int selectedColumn, int playerColor) {
    	// Ensure to keep the index in bounds of the buttons[][]
    	int count = 1;
    	int minColumns = (selectedColumn - 4 >= 0) ? selectedColumn - 4 : 0;
    	int maxColumns = (selectedColumn + 4 < columns) ? selectedColumn + 4 : columns;
    	
    	// checking in direction of -->
		for ( int j = selectedColumn + 1; j < maxColumns ; j++) {
			if (gameMap[selectedRow][j] != playerColor) {
				break;
			}
			count++;
		}
		
		//checking in direction of <-- 
		for ( int j = selectedColumn - 1; j >= minColumns ; j--) {
			if (gameMap[selectedRow][j] != playerColor) {
				break;
			}
			count++;
		}
		
		
		return (count >=4 ) ? "Horizontal" : "";
    }
    
    private String checkForVictory(int selectedRow, int selectedColumn) {
    	/*
    	 * Checks all of the winning terms and returns true if the player won, false otherwise
    	 */
    	return (countVerticalInARow(selectedRow, selectedColumn, playerTurn + 1) +
    			countHorizontalInARow(selectedRow, selectedColumn, playerTurn + 1) +
    			countDescendingDiagonalInARow(selectedRow, selectedColumn, playerTurn + 1) +
    			countAscendingDiagonalInARow(selectedRow, selectedColumn, playerTurn + 1));
    }
    
    private void handleClicked(ActionEvent event) {
    	/*
    	 * Function that is called when clicked on one of the buttons (except clear)
    	 * gets the column number and tries to fill a cell based on whose turn is it
    	 * every turn checks for a win, when a player wins prompts a message and clears the game
    	 */
    	GridButton currentButton = (GridButton)event.getSource();
    	int currentColumn = currentButton.getColumn();
    	int selectedRow = drawCircle(currentColumn);
    	
    	if (selectedRow != -1) {
    		String status = checkForVictory(selectedRow, currentColumn); 
    		if (!status.isEmpty()) {
    			Utils.callInformationAlert(
    					"Player: " + (playerTurn + 1) + " Won!", "Victory!", "Won by: " + status
    					);
    			initialize();
    			return;
    		}
    	}
    	
    	playerTurn = (playerTurn + 1) % 2;  
    }
}