import javafx.scene.control.Button;

public class GridButton extends Button{
	private int color;
	private int row;
	private int column;
	
	public GridButton(String text, int i, int j) {
		super(text);
		setRow(i);
		setColumn(j);
		
		// 0 - No player, 1 - player one, 2 - player 2
		color = 0;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
}
