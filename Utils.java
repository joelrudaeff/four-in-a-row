import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Utils {
	public static Point calcMaximumRowAndMaximumColumn(int selectedRow, int selectedColumn, int maxRows, int maxColumns) {
    	/*
    	 * For loop that returns the maximum (unreachable Point) 
    	 * Point in terms of rows and columns (in a diagonal line)
    	 */
    	int i = 0;
    	
    	for ( i = 1; i < 4 ; i ++) {
    		if (selectedRow + i == maxRows || selectedColumn + i == maxColumns) {
    			break;
    		}
    	}
    	
    	return new Point(selectedRow + i, selectedColumn + i);
    }
    
    public static Point calcMinimumRowAndMinimumColumn(int selectedRow, int selectedColumn, int maxRows, int maxColumns) {
    	/*
    	 * For loop that returns the minimum (unreachable Point) 
    	 * Point in terms of rows and columns (in a diagonal line)
    	 */
    	int i = 0;
    	
    	for ( i = 1; i < 4 ; i ++) {
    		if (selectedRow - i == -1 || selectedColumn - i == -1) {
    			break;
    		}
    	}
    	
    	return new Point(selectedRow - i, selectedColumn - i);
    }
    
    public static Point calcMaximumRowAndMinimumColumn(int selectedRow, int selectedColumn, int maxRows, int maxColumns) {
    	/*
    	 * For loop that returns a (unreachable Point) Point in diagonal line that has
    	 * the maximum row number and minimum column 
    	 */
    	int i = 0;
    	
    	for ( i = 1; i < 4 ; i ++) {
    		if (selectedRow + i == maxRows || selectedColumn - i == -1) {
    			break;
    		}
    	}
    	
    	return new Point(selectedRow + i, selectedColumn - i);
    }
    
    public static Point calcMinimumRowAndMaximumColumn(int selectedRow, int selectedColumn, int maxRows, int maxColumns) {
    	/*
    	 * For loop that returns a (unreachable Point) Point in diagonal line that has
    	 * the minimum row number and maximum column 
    	 */
    	int i = 0;
    	
    	for ( i = 1; i < 4 ; i++) {
    		if (selectedRow - i == -1 || selectedColumn + i == maxColumns) {
    			break;
    		}
    	}
    	
    	return new Point(selectedRow - i, selectedColumn + i);
    }
    
    public static void callInformationAlert(String title, String header, String content) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
    }
}
