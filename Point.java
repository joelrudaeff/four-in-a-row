
public class Point {
	private int x;
	private int y;
	
	public Point(int x, int y) {
		setX(x);
		setY(y);
	}
	
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public boolean equals(Object e) {
		Point other = (Point)e;

		if (x == other.getX() && y == other.getY()) {
			return true;
		}
		
		return false;
	}
}
